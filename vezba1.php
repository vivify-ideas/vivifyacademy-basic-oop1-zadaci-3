<?php

public function dodajFolder(Folder $folder)
{
   $this->lista_foldera[] = $folder;
}

public function dodajFajl(Fajl $fajl)
{
   $this->lista_fajlova[] = $fajl;
}

public function getPovrsina(): float
{
   return $this->povrsina;
}

public function getObim(): float
{
   return $this->obim;
}

public function getZapremina(): float
{
   return $this->zapremina;
}
